#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import datetime
import rsa
import json
import os
import sys
# stgeanography
from stegano import lsb

license_config = {}

license_config['IMG_PATH'] = "image001s.png"
license_config['LICENSE_KEY'] = "123123123"
license_config['LICENSE_DOMAIN'] = "112"


def check_license():
    # By default we suggest that license is invalid
    license_is_valid = False
    startapp = False
    # getting current info
    conf = license_config
    img_path = conf.get('IMG_PATH')
    current_domain = conf.get('LICENSE_DOMAIN')
    current_key = conf.get('LICENSE_KEY')

    # If the image with stored data is absent - don't start the app
    if not os.path.exists(img_path):
        raise Exception('App is corrupted, cannot start')

    # check license info from a saved image
    try:
        license_state = json.loads(lsb.reveal(img_path))
    except json.decoder.JSONDecodeError as e:
        # if there's no data, create default dict and save it 
        state_dict = {'activation_state': 'deactivated', 'license_domain': domain, 'license_key': key}
        secret = lsb.hide(img_path, json.dumps(state_dict))
        secret.save(img_path)
        raise Exception('App is corrupted, cannot start')

    # Saved info
    activation_state = license_state['activation_state']
    license_domain = license_state['license_domain']
    license_key = license_state['license_key']

    # Check if the saved license stats is activated for current domain
    if license_domain == current_domain and activation_state == 'activated' and license_key == current_key:
        license_is_valid = True
        
    return license_is_valid
    


