#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import click
import datetime
import pprint
import rsa
import json
# stgeanography
from stegano import lsb


pp = pprint.PrettyPrinter(indent=4)

config = {}
config['SECRET_KEY'] = 'Change it in production!'
config['LICENSE_SECRET'] = "5b8234797cb801.49457588"
config['LICENSE_KEY'] = "123123123"
config['LICENSE_DOMAIN'] = "112"
config['LICENSE_URL'] = "https://www.enviral-design.com"
config['IMG_PATH'] = "image001s.png"

# The values can be slm_create_new or slm_activate or slm_deactivate
# it changes the 'registered_domains' value:
# if a license is deactivated, the 'registered_domains' is an empty list.
# else it returns a non-empty list with a dict with a number how many times a license was activated
# for this domain
# 'registered_domains': [   {   'id': '10',
#                                  'item_reference': '',
#                                  'lic_key': '5b894dc1b2589',
#                                  'lic_key_id': '2',
#                                  'registered_domain': 'test123'}],
#

# the workflow:
# user activtes the license,
# the PC id is stored in the registered domain field
# the ID can be obtained once on license purchasing,
# it sends a server request to activate the license and at this time
# the domain (the ID) and the license state is stored in the picture with steganography.
# pretend thet someone has copied an entire installation dir to another PC
# we should check if the current ID is the same as we have in the picture.
# this check should be performed on every app run. 

# slm_create_new - allows to create a new license

@click.command()                                                              
@click.option('--action', help='Activate/deactivate/check license')
@click.option('--domain', help='Domain the license is binded')
def license(action, domain):
    """Manage user licenses from CLI"""

    # we store license key and PC UID in an image
    # on every start the app checks if it is started on the same machine
    # that the license has been activated

    # the domain validation is performed on the server side
    # 1. App start
    conf = config
    url = conf.get('LICENSE_URL')
    img_path = conf.get('IMG_PATH')
    
    # we store a json string in an image file:
    # 
    # state_dict = {'activation_state': 'activated', 'license_domain':'111'}
    # json_str = json.dumps(state_dict)
    # secret = lsb.hide("../image001s.png", json.dumps(state_dict))
    # secret.save('../image001s.png')
    # return 
    # license_state = json.loads(json_str)
    # STOPPED HERE >>>

    # if image is corrupted and contains no data,
    # don't start, raise an error
    # 2. App loads saved license and domain data from the local file
    try:
        license_state = json.loads(lsb.reveal(img_path))
    except json.decoder.JSONDecodeError as e:
        # if there's no data, create default dict and save it 
        state_dict = {'activation_state': 'deactivated', 'license_domain': domain}
        secret = lsb.hide(img_path, json.dumps(state_dict))
        secret.save(img_path)
        raise Exception('App is corrupted, cannot start')

    print('Saved license state')
    pp.pprint(license_state)

    activation_state = license_state['activation_state']
    license_domain = license_state['license_domain']

    if license_domain != domain and activation_state != 'deactivated':
        # if there's a domain mismatch and the license is activated for another domain
        print("LOCAL DOMAIN CHECK")
        raise Exception('You cannot run the app on this domain, deactivate it first for the domain {}'.format(license_domain))

    if action == 'run':
        if license_domain == domain and activation_state == 'activated':
            print("Starting app for the domain {}".format(domain))
        else:
            if activation_state == 'deactivated':
                print("App is deactivated for the domain {}".format(license_domain))
            if license_domain != domain:
                print("Domain mismatch license: {}, current: {}".format(license_domain, domain))
        return
        
    # perform license action request
    # parse results and modify license_state depending on
    # response status
    postData = {
        'slm_action':'slm_{}'.format(action),
        'secret_key':conf.get('LICENSE_SECRET'),
        'license_key':conf.get('LICENSE_KEY'),
        'registered_domain':domain
    }
    
    r = requests.post(url, data=postData)
    response = r.json()
    pp.pprint(response)

    if response['result'] == 'success':
        if action != 'check':
            # save new license state and license domain to the image
            license_state['license_domain'] = domain
            license_state['activation_state'] = action+'d'
            
            secret = lsb.hide(img_path, json.dumps(license_state))
            secret.save(img_path)

    else:
        print("REMOTE DOMAIN CHECK")
        raise Exception('Something went wrong', response['message'])



# TODO: Create a Python decorator that would
# wrap arount some main function that starts the app
# and would run the app if the license is valid
# and would drop and error if not.

class MockApp():
    def run():
        print('App starting')


if __name__ == "__main__":
    license()    
