#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import datetime
import rsa
import json
import os
import sys
import pprint
import datetime
import re
# stgeanography
from stegano import lsb
# system info and UUID
import psutil
import uuid
# hashing
import hashlib
#logging
import logging

# Some initial settings
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('"[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"')
ch.setFormatter(formatter)
logger.addHandler(ch)
#

pp = pprint.PrettyPrinter(indent=2)

license_config = {}
license_config['IMG_PATH'] = "defaultimage_s.png"
license_config['LICENSE_KEY'] = "123123123"
license_config['LICENSE_ACTIVATION_SECRET'] = "5b8234797cb801.49457588"
license_config['LICENSE_CREATION_SECRET'] = "5b8234797cb6f5.51898118"
license_config['LICENSE_DOMAIN'] = "112"
license_config['LICENSE_TYPE'] = "GeoPix"
license_config['LICENSE_URL'] = "https://www.enviral-design.com"

# ! TODO:
# Find a way to create unique domain id
# and replace license domain with this value
# Something with MAC address + hardware info hash
# There's no option to retrieve HDD serial info or OS serial (for Windows)
# without root permissions

# Workflow:
# Client provides credentials like email/etc
# gets a license key
# activates it on his machine with first domain
# ...
# deactivates on the frist domain
# activates for another domain

# to delete license: action = delete_license

def get_domain_id(imgpath):
    """
    A function to identify a domain
    Should be a hash from (motherboard_id + MAC address + CPU Info + Other available hardware info)
    MAC: print (':'.join(re.findall('..', '%012x' % uuid.getnode())))
    uuid.UUID(int=uuid.getnode())
    For the first run should create an UUID for the host and write it to the image
    Later it would be used to uniquely identify this host

    As a first approach let it be a hash of user's name/email + MAC address
    Also total HDD volume and CPU
    Here is a lib to collect the required info https://psutil.readthedocs.io/en/latest/
    psutil lib for obtaining hardware info
    """

    # collecting system info:
    maxcpufreq = psutil.cpu_freq().max
    totalmemory = psutil.virtual_memory().total
    totalhdd = psutil.disk_usage('/').total
    mac_address = ':'.join(re.findall('..', '%012x' % uuid.getnode()))
    
    sysinfo = { 'cpu' : maxcpufreq,
                'ram' : totalmemory,
                'hdd' : totalhdd,
                'mac' : mac_address
                }
    h = hashlib.md5()
    h.update(''.join(map(str, [v for v in sysinfo.values()])).encode('utf-8'))
    hashstr = h.hexdigest()
    
    sysinfo['hash'] = hashstr
    
    logger.debug(sysinfo)
    
    return sysinfo

def save_info(imgpath, data):
    logger.debug('Saving data {}'.format(data))
    secret = lsb.hide(imgpath, json.dumps(data))
    secret.save(imgpath)
    

def create_secret(imgpath, data=None):
    """
    A function to fill a given image with initial hidden data
    """

    state_dict = {'activation_state': 'deactivated',
                  'license_domain': None,
                  'license_type': None,
                  'activations_count': 0}


    if data:
        state_dict['license_domain'] = data['hash']
        state_dict.update(data)
        
        logger.debug('Creating secret {}'.format(state_dict))

        save_info(imgpath, state_dict)
    
    
def read_secret(imgpath):
    secret_data = None
    if not os.path.exists(imgpath):
        raise Exception('No such file %s' % imgpath)

    try:
        secret_data = json.loads(lsb.reveal(imgpath))
    except json.decoder.JSONDecodeError as e:
        raise Exception('No hidden data in %s' % imgpath)

    if secret_data:
        logger.debug(secret_data)
        
        return secret_data

    
def update_secret(imgpath, data):
    secret_data = None
    
    if not os.path.exists(imgpath):
        raise Exception('No such file %s' % imgpath)
    try:
        secret_datae = json.loads(lsb.reveal(imgpath))
    except json.decoder.JSONDecodeError as e:
        raise Exception('No hidden data in %s' % imgpath)
    if secret_data:
        logger_debug("Old data: {}".format(secret_data))
        secret_data.update(data)
        logger_debug("New data: {}".format(secret_data))
        save_data(imgpath, secret_data)

    
def create_license(fname, lname, email, cname):

    today = datetime.date.today()
    # license is valid for one year
    nextyear = today.replace(year = today.year + 1)

    postData = {
        'slm_action':'slm_create_new',
        'secret_key': license_config['LICENSE_CREATION_SECRET'],
        'first_name': fname, 
        'last_name': lname,
        'email': email,
        'company_name': cname,
        'txn_id': 1,
        'max_allowed_domains': 1,
        'date_created': str(today),
        'date_expiry': str(nextyear),
        'product_ref': license_config['LICENSE_TYPE']
    }

    imgpath = license_config['IMG_PATH']
    url = license_config['LICENSE_URL']
    r = requests.post(url, data=postData)
    response = r.json()
    logger.debug('Server response: {}'.format(response))
    if response['result'] == 'success':
        # Here we fill the image with default info on license purchase
        # TODO Create default image with empty values
        # On the first app run fill it with domain UUID
        license_state = read_secret(imgpath)
        state_dict = {'activation_state': 'deactivated',
                      'license_type': license_config['LICENSE_TYPE'],
                      'license_key': response['key']
                      }
        license_state.update(state_dict)
        save_info(imgpath, license_state)
        logger.debug(f"Created license: {license_state}")
        return response['key']
    

# TODO: !Replace ldomain from 'default' to a real value
def manage_license(lkey, imgpath, ldomain="default", action="check"):
    # 1. check the img path
    # 2. if path not exists: raise error, app is corrupted
    # 3. else:
    # if there's no license info
    # the app is activated for the first time
    # so we send a server request, check the response and
    # if the license is valid,
    # we save the activation state to the img
    new_activation = False
    license_state = None
    
    if not os.path.exists(imgpath):
        raise Exception('App is corrupted, cannot start')

    try:
        license_state = json.loads(lsb.reveal(imgpath))
    except json.decoder.JSONDecodeError as e:
        # if there's no data, create default dict and save it
        #
        raise Exception('App is corrupted, cannot start')

    # First activation
    if license_state and license_state['activations_count'] == 0:
        # send the server request - check
        # default domain should be something like "default" string
        # get the results 
        # depending on it activate or deactivate the app
        # save license state to the img
        # perform license action request
        # parse results and modify license_state depending on
        # response status
        # The secret key for license verifiaction requests is the same
        # for all the licenses
        postData = {
        'slm_action':'slm_{}'.format(action),
        'secret_key': license_config['LICENSE_ACTIVATION_SECRET'],
        'license_key': lkey,
        'registered_domain':ldomain
    }
        url = license_config['LICENSE_URL']
        r = requests.post(url, data=postData)
        response = r.json()
        logger.debug('Server response: {}'.format(response))
        if response['result'] == 'success':
            # update license state
            license_state['activation_state'] =  'activated'
            license_state['activations_count'] = license_state['activations_count'] + 1
            logger.debug('First activation: {}'.format(license_state))
            save_info(imgpath, license_state)

    elif license_state['activations_count'] > 0:
        # TODO: Activate/deactivate HERE
        if action not in ['activate', 'deactivate', 'check']:
            raise f"There's no such action {action}"

        postData = {
        'slm_action':'slm_{}'.format(action),
        'secret_key': license_config['LICENSE_ACTIVATION_SECRET'],
        'license_key': lkey,
        'registered_domain':ldomain
    }
        url = license_config['LICENSE_URL']
        r = requests.post(url, data=postData)
        response = r.json()
        logger.debug(f'Server response: {response}')
        if response['result'] == 'success':
            # update license state
            if action in ['activate', 'deactivate']:
                license_state['activation_state'] =  f'{action}d'
                if action == 'activate':
                    license_state['activations_count'] = license_state['activations_count'] + 1
                logger.debug('Activation: {}'.format(license_state['activations_count']))
                save_info(imgpath, license_state)
            elif action == 'check':
                logger.debug(f'Checked remote license state {response}')
                


def check_license_local():
    # By default we suggest that license is invalid
    license_is_valid = False
    startapp = False
    # getting current info
    conf = license_config
    img_path = conf.get('IMG_PATH')
    current_domain = conf.get('LICENSE_DOMAIN')
    current_key = conf.get('LICENSE_KEY')
    license_state = None
    # If the image with stored data is absent - don't start the app
    if not os.path.exists(img_path):
        raise Exception('App is corrupted, cannot start')

    # check license info from a saved image
    try:
        license_state = json.loads(lsb.reveal(img_path))

    except json.decoder.JSONDecodeError as e:
        # if there's no data, create default dict and save it 
        # state_dict = {'activation_state': 'deactivated', 'license_domain': domain, 'license_key': key}
        # save_info(img_path, state_dict)
        # We no longer create default empty dict here,
        # instead use save_info
        raise Exception('App is corrupted, cannot start')
    if license_state:
        logger.debug(f'Retrieved local license info: {license_state}')
        # Saved info
        activation_state = license_state['activation_state']
        license_domain = license_state['license_domain']
        license_key = license_state['license_key']

        # Check if the saved license stats is activated for current domain
        logger.debug(f"{license_domain} {current_domain}")
        logger.debug(f"{activation_state}")
        logger.debug(f"{license_key} {current_key}")
        if license_domain == current_domain and activation_state == 'activated' and license_key == current_key:
            license_is_valid = True
        
        return license_is_valid
    
# * * * TESTS * * *
# 0. First run after install: Get current domain ID
domaindata = get_domain_id(license_config['IMG_PATH'])
# set current domain
license_config['LICENSE_DOMAIN'] = domaindata['hash']
logger.debug('Current domain: {}'.format(domaindata))

# 0.1 First run after install: Create secret info
logger.debug('Creating secret info')
create_secret(license_config['IMG_PATH'], data=domaindata)

# 1. Create new license
license_key = create_license('testuser', 'testtest', 'test@example.com', 'MyCompany')
# set license key
license_config['LICENSE_KEY'] = license_key
logger.debug(f'Created license: {license_key}')

# 2. Activate new license
manage_license(license_key, imgpath = license_config['IMG_PATH'], ldomain=license_config['LICENSE_DOMAIN'], action = 'activate')
# after that we can do whatever we want with the new license

# 3. Check license
# retrieve server and local info
valid = check_license_local()
if valid:
    logger.debug('Local license check passed')

# 4. Deactivation of the license for the current domain
manage_license(license_key, imgpath = license_config['IMG_PATH'], ldomain=license_config['LICENSE_DOMAIN'], action = 'deactivate')

# 5. Activate it again
manage_license(license_key, imgpath = license_config['IMG_PATH'], ldomain=license_config['LICENSE_DOMAIN'], action = 'activate')

# 6. Check remote license state
manage_license(license_key, imgpath = license_config['IMG_PATH'], ldomain=license_config['LICENSE_DOMAIN'], action = 'check')

# Check secret info
# read_secret(license_config['IMG_PATH'])
#
# Deactivate license for the current domain
# activate it for another domain
