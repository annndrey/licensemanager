#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import click
import datetime
from flask import Flask


app = Flask(__name__)
app.config['SECRET_KEY'] = 'Change it in production!'

app.config['LICENSE_SECRET'] = "5b8234797cb801.49457588"
app.config['LICENSE_KEY'] = "123123123"
app.config['LICENSE_DOMAIN'] = "NEWDOMAIN"
app.config['LICENSE_URL'] = "https://www.enviral-design.com"


@app.cli.command()                                                              
@click.option('--action', help='Activate/deactivate license')                              
def license_action(action):
    conf = app.config
    url = conf.get('LICENSE_URL')
    postData = {
        'slm_action':'slm_{}'.format(action),
        'secret_key':conf.get('LICENSE_SECRET'),
        'license_key':conf.get('LICENSE_KEY'),
        'registered_domain':conf.get('LICENSE_DOMAIN')
    }
    
    r = requests.post(url, data=postData)
    
    print(r.url)
    print(r.text)
    print(r.json())

